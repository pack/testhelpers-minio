# testhelpers-minio

This is a [testhelper](https://bitbucket.org/pack/testhelpers) for running [minio](https://www.minio.io/) containers to mock `s3` for local development.

## Usage

```go
// in some _test.go file
import (
  "testing"

  "bitbucket.org/pack/testhelpers-minio"
)

func TestMyApp(t *testing.T) {
  th := &thminio.MinioTestHelper{}
  _, err := th.Connect(t)
  defer th.Cleanup()
  if err != nil {
    t.Error(err)
  }
}
```
