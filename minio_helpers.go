package thminio

import (
	"encoding/hex"
	"fmt"
	"testing"

	"bitbucket.org/pack/testhelpers"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

var (
	DefaultMinioImage  = "minio/minio"
	DefaultMinioTag    = "RELEASE.2017-08-05T00-00-53Z-aarch64"
	DefaultMinioRegion = "us-west-2"
	minioTestTags      = []string{"minio_testhelper"} // Note: this is used for test cleanup
	shaLength          = 24                           // default length for sha random bytes
)

type MinioTestHelper struct {
	// Docker configurations
	Image string
	Tag   string

	// S3 configuration
	Session      *session.Session
	Config       *aws.Config
	AccessKey    string
	AccessSecret string
	Region       string

	// Clients
	Container *testhelpers.DockerContainer
	Client    *s3.S3
}

func (th *MinioTestHelper) generateRandomSha1() (string, error) {
	shaSum, err := testhelpers.GenerateRandomSha1(shaLength)
	if err != nil {
		return "", err
	}

	return hex.EncodeToString(shaSum[:]), nil
}

// DockerEnv returns the environment structure for a minio container.
func (th *MinioTestHelper) DockerEnv() []string {
	return []string{
		fmt.Sprintf("MINIO_ACCESS_KEY=%s", th.AccessKey),
		fmt.Sprintf("MINIO_ACCESS_SECRET=%s", th.AccessSecret),
		fmt.Sprintf("MINIO_REGION=%s", th.Region),
	}
}

// SetupDefaults will configure the default variables for a new minio docker container.
func (th *MinioTestHelper) SetupDefaults() (err error) {
	if th.Image == "" {
		th.Image = DefaultMinioImage
	}

	if th.Tag == "" {
		th.Tag = DefaultMinioTag
	}

	if th.Region == "" {
		th.Region = DefaultMinioRegion
	}

	shaSum, err := th.generateRandomSha1()
	if err != nil {
		return err
	}
	th.AccessKey = shaSum[:20]

	th.AccessSecret, err = th.generateRandomSha1()
	return err
}

// Connect will attempt (with retries) to connect to the docker minio test client.
func (th *MinioTestHelper) Connect(t *testing.T) (client *s3.S3, err error) {
	err = th.SetupDefaults()
	if err != nil {
		return nil, err
	}

	cont, err := testhelpers.NewDockerContainer(t,
		fmt.Sprintf("%s:%s", th.Image, th.Tag),
		th.DockerEnv(),
		[]string{},
		minioTestTags,
	)
	if err != nil {
		return nil, err
	}

	err = cont.Retry(func() error {
		portMap, err := cont.PortMapFor(9000)
		if err != nil {
			return err
		}

		th.Config = &aws.Config{
			Credentials:      credentials.NewStaticCredentials(th.AccessKey, th.AccessSecret, ""),
			Endpoint:         aws.String(fmt.Sprintf("http://%s:%d", portMap.ExternalHost(), portMap.HostPort)),
			Region:           aws.String(th.Region),
			DisableSSL:       aws.Bool(true),
			S3ForcePathStyle: aws.Bool(true),
		}

		th.Session = session.New(th.Config)
		th.Client = s3.New(th.Session)
		return err
	})

	return th.Client, err
}

// Cleanup will remove the container if it exists.
func (th *MinioTestHelper) Cleanup() {
	if th.Container != nil {
		th.Container.Remove()
	}
}
