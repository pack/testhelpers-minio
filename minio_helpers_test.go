package thminio

import (
	"testing"
)

func TestMinioConnection(t *testing.T) {
	t.Parallel()

	th := &MinioTestHelper{}
	_, err := th.Connect(t)
	defer th.Cleanup()
	if err != nil {
		t.Error(err)
	}
}
